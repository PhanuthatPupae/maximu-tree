package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEasyMaxSum(t *testing.T) {
	result, err := MaxSum("easy.json")
	if err != nil {
		t.Errorf("failed %v", err)
	}
	assert.Equal(t, 237, result)
}

func TestHardMaxSum(t *testing.T) {
	result, err := MaxSum("hard.json")
	if err != nil {
		t.Errorf("failed %v", err)
	}
	assert.Equal(t, 7273, result)
}
