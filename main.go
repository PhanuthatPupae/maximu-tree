package main

import (
	"encoding/json"
	"fmt"
	"math"
	"os"
)

func main() {
	result, err := MaxSum("easy.json")
	if err != nil {
		return
	}

	fmt.Println(result, 237, result == 237)
}

func MaxSum(pathFile string) (int, error) {
	result := 0

	file, err := os.Open(pathFile)
	if err != nil {
		return result, err
	}
	defer file.Close()

	jsonDecoder := json.NewDecoder(file)

	data := [][]int{}
	err = jsonDecoder.Decode(&data)
	if err != nil {
		return result, err
	}
	return MaxTrigerBottomToTop(data), nil

}

/* 
ref leetcode 120. Triangle
*/
func MaxTrigerBottomToTop(triangle [][]int) int {
	//init array plus 1 for fix out of range
	result := make([]int, len(triangle)+1)
	for i := len(triangle) - 1; i >= 0; i-- {
		for idx, n := range triangle[i] {
			// find max with 2 position
			result[idx] = n + int(math.Max(float64(result[idx]), float64(result[idx+1])))
		}
	}
	return result[0]
}

//TODO
//MaxTopToBottom
